﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class Player : MovingObject {

    public int wallDamage = 1;
    public int pointsPerFood = 10;
    public int pointsPerSoda = 20;
    public float restartLevelDelay = 1f;
    public Text foodText;                       //UI Text to display current player food total.
    public Text wowText;                        //UI Text to display when player has picked up the key.
    public AudioClip moveSound1;                //1 of 2 Audio clips to play when player moves.
    public AudioClip moveSound2;                //2 of 2 Audio clips to play when player moves.
    public AudioClip eatSound1;                 //1 of 2 Audio clips to play when player collects a food object.
    public AudioClip eatSound2;                 //2 of 2 Audio clips to play when player collects a food object.
    public AudioClip drinkSound1;               //1 of 2 Audio clips to play when player collects a soda object.
    public AudioClip drinkSound2;               //2 of 2 Audio clips to play when player collects a soda object.
    public AudioClip gameOverSound;				//Audio clip to play when player dies.
    public AudioClip wowKeyPickupSound;         //Audio clip to play when player picks up 'wow' key.
    public AudioClip badFood;                   //Audio clip to play when player eats harmful food.
    //public AudioClip nextLevel;                 //Audio clip to play when player goes on to next level.

    private Animator animator;
    private int food;
    private bool WOW;

	// Use this for initialization
	protected override void Start ()
    {
        animator = GetComponent<Animator>();

        food = GameManager.instance.playerFoodPoints;

        foodText.text = "Energy: " + food;

        wowText.enabled = false;

        base.Start();

	}

    private void onDisable()
    {
        GameManager.instance.playerFoodPoints = food;
    }
	
	// Update is called once per frame
	private void Update ()
    {
        if (!GameManager.instance.playersTurn)
            return;

        int horizontal = 0;
        int vertical = 0;

        horizontal = (int)Input.GetAxisRaw("Horizontal");
        vertical = (int)Input.GetAxisRaw("Vertical");

        if (horizontal != 0)
        {
            vertical = 0;
        }


        if (horizontal != 0 || vertical != 0)
        {
            AttemptMove<Wall>(horizontal, vertical);
        }
            
	}

    protected override void AttemptMove<T> (int xDir, int yDir)
    {

        int level = GameManager.instance.level;
        if (level < 3)
            food--;
        else if (level >= 3 && level < 5)
            food = food - 2;
        else if (level >= 5 && level < 9)
            food = food - 3;
        else if (level >= 9 && level < 12)
            food = food - 4;
        else if (level >= 12 && level < 30)
            food = food - 5;

        foodText.text = "Energy: " + food;

        base.AttemptMove<T>(xDir, yDir);

        RaycastHit2D hit;

        if (Move(xDir, yDir, out hit))
        {
            //Call RandomizeSfx of SoundManager to play the move sound, passing in two audio clips to choose from.
            
            SoundManager.instance.RandomizeSfx(moveSound1, moveSound2);

        }

        CheckIfGameOver();

        GameManager.instance.playersTurn = false;
    }


    protected override void OnCantMove <T> (T component)
    {
        //Set hitWall to equal the component passed in as a parameter.
        Wall hitWall = component as Wall;

        //Call the DamageWall function of the Wall we are hitting.
        //hitWall.DamageWall(wallDamage);

        //Set the attack trigger of the player's animation controller in order to play the player's attack animation.
        animator.SetTrigger("PlayerChop");
    }



    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Exit" && WOW)
        {
            WOW = false;
            Invoke("Restart", restartLevelDelay);
            enabled = false;
        }
        else if (other.tag == "Food")
        {
            food += pointsPerFood;


            //Update foodText to represent current total and notify player that they gained points
            foodText.text = "+" + pointsPerFood + " Energy: " + food;

            //Call the RandomizeSfx function of SoundManager and pass in two eating sounds to choose between to play the eating sound effect.
            SoundManager.instance.RandomizeSfx(eatSound1, eatSound2);

            other.gameObject.SetActive(false);
        }
        else if (other.tag == "Soda")
        {
            food += pointsPerSoda;

            //Update foodText to represent current total and notify player that they gained points
            foodText.text = "+" + pointsPerSoda + " Energy: " + food;

            //Call the RandomizeSfx function of SoundManager and pass in two drinking sounds to choose between to play the drinking sound effect.
            SoundManager.instance.RandomizeSfx(drinkSound1, drinkSound2);

            other.gameObject.SetActive(false);
        }
        else if (other.tag == "BadFood")
        {
            animator.SetTrigger("PlayerHit");
            food -= pointsPerFood;
            foodText.text = "Energy: " + food;
            SoundManager.instance.RandomizeSfx(badFood);
            other.gameObject.SetActive(false);
        }
        else if (other.tag == "WowTile")
        {
            other.gameObject.SetActive(false);
            WOW = true;
            wowText.enabled = true;
            SoundManager.instance.PlaySingle(wowKeyPickupSound);
        }
    
    }

    //Restart reloads the scene when called.
    private void Restart()
    {
        //Load the last scene loaded, in this case Main, the only scene in the game. And we load it in "Single" mode so it replace the existing one
        //and not load all the scene object in the current scene.
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
    }

    public void LoseFood (int loss)
    {
        animator.SetTrigger("PlayerHit");
        food -= loss;

        //Update the food display with the new total.
        foodText.text = "-" + loss + " Energy: " + food;

        CheckIfGameOver();
    }

    private void CheckIfGameOver()
    {
        if (food <= 0)
        {
            SoundManager.instance.PlaySingle(gameOverSound);
            SoundManager.instance.musicSource.Stop();
        

        GameManager.instance.GameOver();
    }
    }

}
