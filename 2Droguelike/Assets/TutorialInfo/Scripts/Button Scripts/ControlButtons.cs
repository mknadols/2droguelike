﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControlButtons : MonoBehaviour {

    public Button menuButton;

    // Use this for initialization
    void Start () {

       menuButton = menuButton.GetComponent<Button>();

    }

    public void MenuReturn()
    {
        SceneManager.LoadScene(0);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
