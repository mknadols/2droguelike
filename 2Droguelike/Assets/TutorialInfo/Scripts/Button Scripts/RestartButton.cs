﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RestartButton: MonoBehaviour {

    public Button restartButton;

	// Use this for initialization
	void Start () {
        restartButton = restartButton.GetComponent<Button>();
	}

    public void RestartLevel()
    {
        GameManager.instance.level = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        SoundManager.instance.musicSource.Play();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
