﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour {

    public Button playButton;
    public Button controlButton;

	// Use this for initialization
	void Start () {

        playButton = playButton.GetComponent<Button>();

        controlButton = controlButton.GetComponent<Button>();

	}

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }
	
    public void ControlMenu()
    {
        SceneManager.LoadScene(2);
    }

	// Update is called once per frame
	void Update () {
		
	}
}
